﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeApi.Models
{
    [Table("Employee")]
    public class Employee
    {
        [Key]
        public string EmployeeNo { get; set; }
        [Required]
        public string EmployeeName { get; set; } = string.Empty;
        public string EmployeeStatus { get; set; } = string.Empty;
    }
}
