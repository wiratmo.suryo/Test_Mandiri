using EmployeeApi.Interface;
using EmployeeApi.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository repository;

        public EmployeeController(IEmployeeRepository repository)
        {
            this.repository = repository;
        }
        // GET: api/<EmployeeController>
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            var list = repository.GetAll();
            return list;
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public Employee Get(string id)
        {
            var employee = repository.GetById(id);
            if (employee != null)
            {
                return employee;
            }
            return null;
        }

        // POST api/<EmployeeController>
        [HttpPost]
        public void Post([FromBody] Employee model)
        {
            try
            {
                repository.Insert(model);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }


        }

        // PUT api/<EmployeeController>/5
        [HttpPut]
        public void Put([FromBody] Employee model)
        {
            try
            {
                repository.Update(model);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            try
            {
                repository.Delete(id);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
