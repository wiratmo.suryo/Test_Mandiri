﻿using EmployeeApi.Models;

namespace EmployeeApi.Interface
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAll();
        Employee GetById(string employeeNo);
        void Insert(Employee employee);
        void Update(Employee employee);
        void Delete(string employeeNo);
        void Save();
    }
}
