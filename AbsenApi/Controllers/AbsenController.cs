using AbsenApi.Interface;
using AbsenApi.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AbsenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AbsenController : ControllerBase
    {
        private readonly IAbsenRepository repository;

        public AbsenController(IAbsenRepository repository)
        {
            this.repository = repository;
        }
        // GET: api/<AbsenController>
        [HttpGet]
        public IEnumerable<Absen> Get()
        {
            var list = repository.GetAll();
            return list;
        }

        // GET api/<AbsenController>/5
        [HttpGet("{id}")]
        public Absen Get(long id)
        {
            var Absen = repository.GetById(id);
            if (Absen != null)
            {
                return Absen;
            }
            return null;
        }

        // POST api/<AbsenController>
        [HttpPost]
        public void Post([FromBody] Absen model)
        {
            try
            {
                repository.Insert(model);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }


        }

        // PUT api/<AbsenController>/5
        [HttpPut]
        public void Put([FromBody] Absen model)
        {
            try
            {
                repository.Update(model);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }
        }

        // DELETE api/<AbsenController>/5
        [HttpDelete("{id}")]
        public void Delete(long id)
        {
            try
            {
                repository.Delete(id);
                repository.Save();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
