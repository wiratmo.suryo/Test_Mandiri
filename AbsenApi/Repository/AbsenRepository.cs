﻿using AbsenApi.Interface;
using AbsenApi.Models;
using Microsoft.EntityFrameworkCore;


namespace AbsenApi.Repository
{
    public class AbsenRepository : IAbsenRepository
    {
        private readonly AbsenDbContext _context;

        public AbsenRepository(AbsenDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Absen> GetAll()
        {
            return _context.Absens.ToList();
        }
        public Absen GetById(long absenId)
        {
            return _context.Absens.Find(absenId);
        }

        public void Insert(Absen Absen)
        {  
            _context.Absens.Add(Absen);
        }
        public void Update(Absen Absen)
        { 
            _context.Entry(Absen).State = EntityState.Modified;
        } 
        public void Delete(long absenId)
        { 
            Absen Absen = _context.Absens.Find(absenId); 
            if (Absen != null)
            { 
                _context.Absens.Remove(Absen);
            }

        }
        public void Save()
        {
            _context.SaveChanges();
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
