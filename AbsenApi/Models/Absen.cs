﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AbsenApi.Models
{
    [Table("Absen")]
    public class Absen
    {
        [Key]
        public long AbsenId { get; set; }
        [Required]
        public string EmployeeNo { get; set; } = string.Empty;
        public DateTime ClockIn { get; set; }
        public DateTime ClockOut { get; set; }
    }
}
