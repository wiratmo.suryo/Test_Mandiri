﻿using AbsenApi.Models;

namespace AbsenApi.Interface
{
    public interface IAbsenRepository
    {
        IEnumerable<Absen> GetAll();
        Absen GetById(long absenId);
        void Insert(Absen Absen);
        void Update(Absen Absen);
        void Delete(long absenId);
        void Save();
    }
}
