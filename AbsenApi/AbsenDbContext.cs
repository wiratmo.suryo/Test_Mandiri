﻿using AbsenApi.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace AbsenApi
{
    public class AbsenDbContext : DbContext
    {
        public DbSet<Absen> Absens { get; set; }
        public AbsenDbContext(DbContextOptions<AbsenDbContext> options) : base(options)
        {
        }
    }
}
